CREATE TABLE [dbo].[Movies] (
    [MovieID]   INT            IDENTITY (1, 1) NOT NULL,
    [MovieName] NVARCHAR (100) NULL,
    [Cast]      NVARCHAR (500) NULL,
    [Director]  NVARCHAR (100) NULL,
    [Producer]  NVARCHAR (500) NULL,
    [RDate]     NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([MovieID] ASC)
);